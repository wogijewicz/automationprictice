package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.ProductsPage;
import utils.Navigation;

public class CartTest extends BaseTest {

    @Test
    void shouldBeAbleToAddProductToTheCart() {
        ProductsPage productsPage = Navigation.goToProductCategoryPage(0, driver, wait);
        productsPage.addRandomProductToCart();
        Assertions.assertEquals(1, productsPage.getCartSize());
    }

    @Test
    void shouldBeAbleToAddMultipleProductsToTheCart() {
        ProductsPage productsPage = Navigation.goToProductCategoryPage(1, driver, wait);
        productsPage.addRandomProductToCart();
        productsPage.addRandomProductToCart();
        Assertions.assertEquals(2, productsPage.getCartSize());
    }

    @Test
    void shouldBeAbleToAddProductsToTheCart2() {
        ProductsPage productsPage = Navigation.goToProductCategoryPage(2, driver, wait);
        productsPage.addRandomProductToCart();
        Assertions.assertEquals(1, productsPage.getCartSize());
    }
}
