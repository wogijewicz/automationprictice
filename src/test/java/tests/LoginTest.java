package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.HomePage;
import pageobjects.LoginPage;
import pageobjects.MyAccountPage;

public class LoginTest extends BaseTest {

    @Test
    void shouldRedirectToMyAccountPageWhenCorrectCredentialsAreUsed() {
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.successfulLogin();
        MyAccountPage myAccountPage = new MyAccountPage(driver, wait);
        Assertions.assertTrue(myAccountPage.isUserLoginIn());
    }

    @Test
    void shouldDisplayedAuthenticationFailedAlertWhenEnterWrongPassword() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.openPage();
        LoginPage loginPage = homePage.goToLoginPage();
        loginPage.login("myTest2020@gmail.com", "wrong_password");
        Assertions.assertTrue(loginPage.isAlertTextCorrectWhenEnterWrongPassword());
    }

    @Test
    void shouldDisplayedAuthenticationFailedAlertWhenEnterWrongEmail() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.openPage();
        LoginPage loginPage = homePage.goToLoginPage();
        loginPage.login("wrong_email@gmail.com", "1qaz!QAZ");
        Assertions.assertTrue(loginPage.isAlertTextCorrectWhenEnterWrongEmail());
    }

    @Test
    void shouldDisplayedRequiredAlertWhenPasswordIsNotEnter() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.openPage();
        LoginPage loginPage = homePage.goToLoginPage();
        loginPage.login("myTest2020@gmail.com", "");
        Assertions.assertTrue(loginPage.isPasswordRequiredAlertDisplayed());
    }

    @Test
    void shouldDisplayedRequiredAlertWhenEmailIsNotEnter() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.openPage();
        LoginPage loginPage = homePage.goToLoginPage();
        loginPage.login("", "1qaz!QAZ");
        Assertions.assertTrue(loginPage.isEmailRequiredAlertDisplayed());
    }

    @Test
    void forgotPasswordWithRightEmail() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.openPage();
        LoginPage loginPage = homePage.goToLoginPage();
        loginPage.forgotPassword("myTest2020@gmail.com");
        Assertions.assertTrue(loginPage.isConfirmationEmailHasBeenSentAlertDisplayed());
    }

    @Test
    void forgotPasswordWithNotCorrectEmail() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.openPage();
        LoginPage loginPage = homePage.goToLoginPage();

        loginPage.forgotPassword("khvkhvhk@jbjb.com");
        Assertions.assertTrue(loginPage.isNoAccountRegisteredAlertDisplayed());

        loginPage.emailField("");
        Assertions.assertTrue(loginPage.isInvalidEmailAlertDisplayed());

        loginPage.emailField(" @gmail.com");
        Assertions.assertTrue(loginPage.isInvalidEmailAlertDisplayed());
    }
}
