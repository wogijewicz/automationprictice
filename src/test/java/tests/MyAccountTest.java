package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.LoginPage;
import pageobjects.MyAccountPage;

public class MyAccountTest extends BaseTest {

    @Test
    void shouldDisplayUserPersonalDataCorrectly() {
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.successfulLogin();
        MyAccountPage myAccountPage = new MyAccountPage(driver, wait);
        myAccountPage.myPersonalInformation();
        Assertions.assertTrue(myAccountPage.isUserGenderDisplayCorrectly());
        Assertions.assertTrue(myAccountPage.isUserFirstNameDisplayCorrectly());
        Assertions.assertTrue(myAccountPage.isUserLastNameDisplayCorrectly());
        Assertions.assertTrue(myAccountPage.isUserEmailDisplayCorrectly());
        Assertions.assertTrue(myAccountPage.isUserDayOfBirthDisplayCorrectly());
        Assertions.assertTrue(myAccountPage.isUserMonthOfBirthDisplayCorrectly());
        Assertions.assertTrue(myAccountPage.isUserYearOfBirthDisplayCorrectly());
    }
}
