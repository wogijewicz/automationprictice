package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.*;
import utils.Navigation;

public class WishlistTest extends BaseTest {

    @Test
    void shouldBeAbleToAddProductToTheWishlistWhenUserIsLoggedIn() {
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.successfulLogin();
        ProductsPage productsPage = Navigation.goToProductCategoryPage(0, driver, wait);
        productsPage.addRandomProductToWishlist();
        Assertions.assertTrue(productsPage.isAddedToYourWishlistMessageDisplayed());
    }

    @Test
    void shouldNotBeAbleToAddProductToTheWishlistWhenUserIsNotLoggedIn() {
        ProductsPage productsPage = Navigation.goToProductCategoryPage(0, driver, wait);
        productsPage.addRandomProductToWishlist();
        Assertions.assertTrue(productsPage.isYouMustBeLoggedInToManageYourWishlistAlertDisplayed());
    }

    @Test
    void shouldBeAbleToDisplayAddedProductInTheWishlist() {
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.successfulLogin();

        MyWishlistsPage myWishlistsPage = new MyWishlistsPage(driver, wait);
        myWishlistsPage.checkIfWishlistsEmpty();

        ProductsPage productsPage = Navigation.goToProductCategoryPage(0, driver, wait);
        productsPage.addProductToWishlist(0);
        productsPage.closeFancyboxMessage();

        MyAccountPage myAccountPage = new MyAccountPage(driver, wait);
        myAccountPage.goToWishlistsPage();

        myWishlistsPage.openMyWishlistManage();
        Assertions.assertTrue(myWishlistsPage.isProductAtWishlist("Faded Short Sleeve T-shirts"));

        myWishlistsPage.deleteTheWishlist();
    }
}
