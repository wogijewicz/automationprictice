package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.BasePage;
import pageobjects.LoginPage;
import pageobjects.MyAccountPage;

public class SignOutTest extends BaseTest {

    @Test
    void signOut() {
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.successfulLogin();

        MyAccountPage myAccountPage = new MyAccountPage(driver, wait);
        Assertions.assertTrue(myAccountPage.isUserLoginIn());

        BasePage basePage = new BasePage(driver, wait);
        basePage.signOut();
        Assertions.assertTrue(basePage.isSignInLinkDisplayed());
    }
}