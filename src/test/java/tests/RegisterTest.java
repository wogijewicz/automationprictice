package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.HomePage;
import pageobjects.LoginPage;
import pageobjects.MyAccountPage;
import pageobjects.RegisterPage;
import utils.RandomUser;

public class RegisterTest extends BaseTest {

    @Test
    void shouldRegisterNewUserWhenAllMandatoryDataIsProvided() {
        RandomUser user = new RandomUser();
        System.out.println(user);

        HomePage homePage = new HomePage(driver, wait);
        homePage.openPage();
        LoginPage loginPage = homePage.goToLoginPage();

        RegisterPage registerPage = loginPage.goToRegisterPage();
        registerPage.registerUser(user);
        MyAccountPage myAccountPage = new MyAccountPage(driver, wait);
        Assertions.assertTrue(myAccountPage.isUserLoginIn());
    }

    @Test
    void shouldDisplayedAlertWhenNotAllRequiredFieldsInput() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.openPage();
        LoginPage loginPage = homePage.goToLoginPage();
        RegisterPage registerPage = loginPage.enterNewUserEmail("@gmail.com");
        registerPage.registerNewUser("", "", "", "", "", "", "", "", "");
        Assertions.assertTrue(registerPage.isPhoneNumberRequiredAlertDisplayed());
        Assertions.assertTrue(registerPage.isLastnameRequiredAlertDisplayed());
        Assertions.assertTrue(registerPage.isFirstnameRequiredAlertDisplayed());
        Assertions.assertTrue(registerPage.isPasswordRequiredAlertDisplayed());
        Assertions.assertTrue(registerPage.isAddress1RequiredAlertDisplayed());
        Assertions.assertTrue(registerPage.isCityRequiredAlertDisplayed());
        Assertions.assertTrue(registerPage.isPostCodeInvalidAlertDisplayed());
    }

    @Test
    void shouldDisplayedAlertWhenCountryIsNotInput() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.openPage();
        LoginPage loginPage = homePage.goToLoginPage();
        RegisterPage registerPage = loginPage.enterNewUserEmail("@gmail.com");
        registerPage.registerNewUser("Ann", "Gibson", "sdfgfg", "Suite 037 78910 Flatley Fort", "New-York", "21", "10001", "-", "14436924557");
        Assertions.assertTrue(registerPage.isCountryCannotBeLoadedWithAddressAlertDisplayed());
        Assertions.assertTrue(registerPage.isCountryInvalidAlertDisplayed());
        Assertions.assertTrue(registerPage.isCountryIdRequiredAlertDisplayed());
    }

    @Test
    void shouldDisplayedAlertWhenPostCodeIsNotFiveDigit() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.openPage();
        LoginPage loginPage = homePage.goToLoginPage();
        RegisterPage registerPage = loginPage.enterNewUserEmail("@gmail.com");

        registerPage.registerNewUser("Ann", "Gibson", "sdfgfg", "Suite 037 78910 Flatley Fort", "New-York", "21", "1234", "21", "14436924557");
        Assertions.assertTrue(registerPage.isPostCodeInvalidAlertDisplayed());
        registerPage.clearField();

        registerPage.registerNewUser("Ann", "Gibson", "sdfgfg", "Suite 037 78910 Flatley Fort", "New-York", "21", "123456", "21", "14436924557");
        Assertions.assertTrue(registerPage.isPostCodeInvalidAlertDisplayed());
        registerPage.clearField();

        registerPage.registerNewUser("Ann", "Gibson", "sdfgfg", "Suite 037 78910 Flatley Fort", "New-York", "21", "mlmmdl", "21", "14436924557");
        Assertions.assertTrue(registerPage.isPostCodeInvalidAlertDisplayed());
        registerPage.clearField();

        registerPage.registerNewUser("Ann", "Gibson", "sdfgfg", "Suite 037 78910 Flatley Fort", "New-York", "21", "*****", "21", "14436924557");
        Assertions.assertTrue(registerPage.isPostCodeInvalidAlertDisplayed());
        registerPage.clearField();

        registerPage.registerNewUser("Ann", "Gibson", "sdfgfg", "Suite 037 78910 Flatley Fort", "New-York", "21", "123_5", "21", "14436924557");
        Assertions.assertTrue(registerPage.isPostCodeInvalidAlertDisplayed());
        registerPage.clearField();

        registerPage.registerNewUser("Ann", "Gibson", "sdfgfg", "Suite 037 78910 Flatley Fort", "New-York", "21", "123 5", "21", "14436924557");
        Assertions.assertTrue(registerPage.isPostCodeInvalidAlertDisplayed());
    }
}