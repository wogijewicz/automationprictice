package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.OrderPage;

public class OrderTest extends BaseTest {

    @Test
    void shouldBeAbleToMakeAnOrderWhenCustomerPayByBankWire() {
        OrderPage orderPage = new OrderPage(driver, wait);
        orderPage.makeAnOrder();
        orderPage.bankWirePaymentMethod();
        Assertions.assertTrue(orderPage.isOrderCompleteIfBankWirePayment());
    }

    @Test
    void shouldBeAbleToMakeAnOrderWhenCustomerPayByCheck() {
        OrderPage orderPage = new OrderPage(driver, wait);
        orderPage.makeAnOrder();
        orderPage.checkPaymentMethod();
        Assertions.assertTrue(orderPage.isOrderCompleteIfCheckPayment());
    }
}
