package utils;

import com.github.javafaker.Faker;

public class RandomUser {

    public String firstName;
    public String lastName;
    public String email;
    public String password;
    public int dayOfBirth;
    public int monthOfBirth;
    public int yearOfBirth;
    public String address1;
    public String city;
    public int state;
    public int zipCode;
    public String country;
    public String mobilePhone;

    public RandomUser() {
        Faker faker = new Faker();
        firstName = faker.name().firstName();
        lastName = faker.name().lastName();
        email = firstName + lastName + yearOfBirth + "@gmail.com";
        password = "passwrd";
        dayOfBirth = faker.random().nextInt(31) + 1;
        monthOfBirth = faker.random().nextInt(12) + 1;
        yearOfBirth = faker.random().nextInt(1900, 2020) + 1;
        address1 = faker.address().fullAddress();
        city = faker.address().city();
        state = faker.random().nextInt(50) + 1;
        zipCode = faker.random().nextInt(10000, 99999);
        country = "21";
        mobilePhone = faker.phoneNumber().cellPhone();
    }

    public static void main(String[] args) {
        RandomUser user = new RandomUser();
        System.out.println(user);
    }

    @Override
    public String toString() {
        return "RandomUser{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", password='" + password + '\'' +
                ", dayOfBirth=" + dayOfBirth +
                ", monthOfBirth=" + monthOfBirth +
                ", yearOfBirth=" + yearOfBirth +
                ", address1='" + address1 + '\'' +
                ", city='" + city + '\'' +
                ", state=" + state +
                ", zipCode=" + zipCode +
                ", country='" + country + '\'' +
                ", mobilePhone='" + mobilePhone + '\'' +
                '}';
    }
}
