package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageobjects.HomePage;
import pageobjects.ProductsPage;

public class Navigation {

    public static ProductsPage goToProductCategoryPage(int i, WebDriver driver, WebDriverWait wait) {
        HomePage homePage = new HomePage(driver, wait);
        homePage.openPage();
        homePage.goToProductCategoryByIndex(i);
        return new ProductsPage(driver, wait);
    }
}
