package pageobjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.RandomUser;
import java.util.Random;

public class LoginPage extends BasePage {

    @FindBy(id = "email")
    WebElement loginInputField;

    @FindBy(id = "passwd")
    WebElement passwordInputField;

    @FindBy(css = "#center_column >.alert-danger")
    WebElement alertMessage;

    @FindBy(css = ".center_column .alert-danger")
    WebElement alertMessageAtForgotPasswordPage;

    @FindBy(id = "email_create")
    WebElement registerEmailInputField;

    @FindBy(xpath = "//a[contains(text(),'Forgot your password')]")
    WebElement forgotYourPasswordLink;

    @FindBy(css = ".submit .button-medium")
    WebElement retrievePasswordButton;

    @FindBy(css = ".alert-success")
    WebElement confirmationEmailHasBeenSentAlert;

    public LoginPage(WebDriver driverIn, WebDriverWait waitIn) {
        super(driverIn, waitIn);
    }

    public RegisterPage goToRegisterPage() {
        RandomUser randomUser = new RandomUser();
        String newEmail = randomUser.email;
        registerEmailInputField.sendKeys(newEmail);
        registerEmailInputField.sendKeys(Keys.ENTER);
        System.out.println(newEmail);
        return new RegisterPage(driver, wait);
    }

    public RegisterPage enterNewUserEmail(String emailDomain) {
        Random random = new Random();
        String newEmailForUser = "random_email" + random.nextInt(999999) + emailDomain;
        registerEmailInputField.sendKeys(newEmailForUser);
        registerEmailInputField.sendKeys(Keys.ENTER);
        System.out.println(newEmailForUser);
        return new RegisterPage(driver, wait);
    }

    public void login(String userName, String password) {
        loginInputField.clear();
        loginInputField.sendKeys(userName);
        passwordInputField.clear();
        passwordInputField.sendKeys(password);
        passwordInputField.sendKeys(Keys.ENTER);
    }

    public void successfulLogin() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.openPage();
        homePage.goToLoginPage();
        loginInputField.clear();
        loginInputField.sendKeys(emailForSuccessfulLogin);
        passwordInputField.clear();
        passwordInputField.sendKeys(passwordForSuccessfulLogin);
        passwordInputField.sendKeys(Keys.ENTER);
    }

    public boolean isAlertTextCorrectWhenEnterWrongPassword() {
        String alertText = alertMessage.getText();
        return alertText.equals("There is 1 error\nAuthentication failed.");
    }

    public boolean isAlertTextCorrectWhenEnterWrongEmail() {
        String alertText = alertMessage.getText();
        return alertText.equals("There is 1 error\nAuthentication failed.");
    }

    public boolean isPasswordRequiredAlertDisplayed() {
        String alertText = alertMessage.getText();
        return alertText.equals("There is 1 error\nPassword is required.");
    }

    public boolean isEmailRequiredAlertDisplayed() {
        String alertText = alertMessage.getText();
        return alertText.equals("There is 1 error\nAn email address required.");
    }

    public void forgotPassword(String email) {
        wait.until(ExpectedConditions.elementToBeClickable(forgotYourPasswordLink)).click();
        loginInputField.clear();
        loginInputField.sendKeys(email);
        wait.until(ExpectedConditions.elementToBeClickable(retrievePasswordButton)).click();
    }

    public boolean isConfirmationEmailHasBeenSentAlertDisplayed() {
        return confirmationEmailHasBeenSentAlert.getText().contains("A confirmation email has been sent to your address:");
    }

    public boolean isNoAccountRegisteredAlertDisplayed() {
        String alertText = alertMessageAtForgotPasswordPage.getText();
        return alertText.equals("There is 1 error\nThere is no account registered for this email address.");
    }

    public boolean isInvalidEmailAlertDisplayed() {
        String alertText = alertMessageAtForgotPasswordPage.getText();
        return alertText.equals("There is 1 error\nInvalid email address.");
    }

    public void emailField(String email) {
        loginInputField.clear();
        loginInputField.sendKeys(email);
        wait.until(ExpectedConditions.elementToBeClickable(retrievePasswordButton)).click();
    }
}
