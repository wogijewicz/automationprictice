package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;
import java.util.Random;

public class ProductsPage extends BasePage {

    @FindBy(css = ".product_list .product-container")
    List<WebElement> productContainers;

    @FindBy(css = ".ajax_add_to_cart_button")
    List<WebElement> addToCartButtons;

    @FindBy(className = "continue")
    WebElement continueShoppingButton;

    @FindBy(xpath = "//a[@title='View my shopping cart']")
    WebElement cartContainer;

    @FindBy(id = "button_order_cart")
    WebElement checkOutButton;

    @FindBy(css = ".lnk_view ")
    List<WebElement> moreButton;

    @FindBy(id = "wishlist_button")
    WebElement addToWishlistButton;

    @FindBy(css = ".fancybox-outer")
    WebElement fancyboxMessage;

    @FindBy(css = ".fancybox-close")
    WebElement fancyboxCloseButton;


    public ProductsPage(WebDriver driverIn, WebDriverWait waitIn) {
        super(driverIn, waitIn);
    }

    public void moveMouseToProductContainer(int productIndex) {
        Actions builder = new Actions(driver);
        builder.moveToElement(productContainers.get(productIndex)).build().perform();
    }

    public void addProductToTheBasket(int productIndex) {
        wait.until(ExpectedConditions.elementToBeClickable(addToCartButtons.get(productIndex))).click();
        wait.until(ExpectedConditions.elementToBeClickable(continueShoppingButton)).click();
    }

    public void addRandomProductToCart() {
        Random rnd = new Random();
        int productIndex = rnd.nextInt(productContainers.size());
        moveMouseToProductContainer(productIndex);
        addProductToTheBasket(productIndex);
    }

    public void moveMouseToCartContainer() {
        Actions builder = new Actions(driver);
        builder.moveToElement(cartContainer).build().perform();
    }

    public void goToCartPage() {
        moveMouseToCartContainer();
        wait.until(ExpectedConditions.elementToBeClickable(checkOutButton)).click();
    }

    public void goToProductPage(int productIndex) {
        moveMouseToProductContainer(productIndex);
        wait.until(ExpectedConditions.elementToBeClickable(moreButton.get(productIndex))).click();
    }

    public void addProductToWishlist(int productIndex) {
        goToProductPage(productIndex);
        wait.until(ExpectedConditions.elementToBeClickable(addToWishlistButton)).click();
    }

    public void addRandomProductToWishlist() {
        Random rnd = new Random();
        int productIndex = rnd.nextInt(productContainers.size());
        goToProductPage(productIndex);
        wait.until(ExpectedConditions.elementToBeClickable(addToWishlistButton)).click();
    }

    public boolean isYouMustBeLoggedInToManageYourWishlistAlertDisplayed() {
        return fancyboxMessage.getText().equals("You must be logged in to manage your wishlist.");
    }

    public boolean isAddedToYourWishlistMessageDisplayed() {
        return fancyboxMessage.getText().equals("Added to your wishlist.");
    }

    public void closeFancyboxMessage() {
        wait.until(ExpectedConditions.elementToBeClickable(fancyboxCloseButton)).click();
    }
}
