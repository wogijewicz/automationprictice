package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.RandomUser;

public class RegisterPage extends BasePage {

    @FindBy(id = "id_gender2")
    WebElement genderRadioBox;

    @FindBy(id = "customer_firstname")
    WebElement customerFirstName;

    @FindBy(id = "customer_lastname")
    WebElement customerLastName;

    @FindBy(id = "passwd")
    WebElement password;

    @FindBy(id = "days")
    WebElement customerDayOfBirth;

    @FindBy(id = "months")
    WebElement customerMonthOfBirth;

    @FindBy(id = "years")
    WebElement customerYearOfBirth;

    @FindBy(id = "address1")
    WebElement customerAddress;

    @FindBy(id = "city")
    WebElement customerCity;

    @FindBy(id = "id_state")
    WebElement customerState;

    @FindBy(id = "postcode")
    WebElement customerPostcode;

    @FindBy(id = "id_country")
    WebElement customerCountry;

    @FindBy(id = "phone_mobile")
    WebElement customerMobilePhone;

    @FindBy(id = "submitAccount")
    WebElement submitAccountButton;

    @FindBy(css = "#center_column >.alert-danger")
    WebElement alertMessage;

    public RegisterPage(WebDriver driverIn, WebDriverWait waitIn) {
        super(driverIn, waitIn);
    }

    public void registerUser(RandomUser user) {
        wait.until(ExpectedConditions.elementToBeClickable(genderRadioBox)).click();
        customerFirstName.sendKeys(user.firstName);
        customerLastName.sendKeys(user.lastName);
        password.sendKeys("passwrd");

        Select day = new Select(customerDayOfBirth);
        day.selectByIndex(user.dayOfBirth);

        Select month = new Select(customerMonthOfBirth);
        month.selectByIndex(user.monthOfBirth);

        Select year = new Select(customerYearOfBirth);
        year.selectByValue(String.valueOf(user.yearOfBirth));

        customerAddress.sendKeys(user.address1);
        customerCity.sendKeys(user.city);

        Select state = new Select(customerState);
        state.selectByValue(String.valueOf(user.state));

        customerPostcode.sendKeys(String.valueOf(user.zipCode));

        Select country = new Select(customerCountry);
        country.selectByValue(user.country);

        customerMobilePhone.sendKeys(user.mobilePhone);
        wait.until(ExpectedConditions.elementToBeClickable(submitAccountButton)).click();
    }

    public void registerNewUser(String firstName, String lastName, String userPassword, String address1, String city, String state,
                                String zipCode, String country, String mobilePhone) {

        customerFirstName.sendKeys(firstName);
        customerLastName.sendKeys(lastName);
        password.sendKeys(String.valueOf(userPassword));
        customerAddress.sendKeys(address1);
        customerCity.sendKeys(city);
        Select state2 = new Select(customerState);
        state2.selectByValue(state);
        customerPostcode.sendKeys(zipCode);
        customerCountry.sendKeys(country);
        customerMobilePhone.sendKeys(mobilePhone);
        wait.until(ExpectedConditions.elementToBeClickable(submitAccountButton)).click();
    }

    public boolean isPhoneNumberRequiredAlertDisplayed() {
        String alertText = alertMessage.getText();
        return alertText.contains("You must register at least one phone number.");
    }

    public boolean isLastnameRequiredAlertDisplayed() {
        String alertText = alertMessage.getText();
        return alertText.contains("lastname is required.");
    }

    public boolean isFirstnameRequiredAlertDisplayed() {
        String alertText = alertMessage.getText();
        return alertText.contains("firstname is required.");
    }

    public boolean isPasswordRequiredAlertDisplayed() {
        String alertText = alertMessage.getText();
        return alertText.contains("passwd is required.");
    }

    public boolean isAddress1RequiredAlertDisplayed() {
        String alertText = alertMessage.getText();
        return alertText.contains("address1 is required.");
    }

    public boolean isCityRequiredAlertDisplayed() {
        String alertText = alertMessage.getText();
        return alertText.contains("city is required.");
    }

    public boolean isPostCodeInvalidAlertDisplayed() {
        String alertText = alertMessage.getText();
        return alertText.contains("The Zip/Postal code you've entered is invalid. It must follow this format: 00000");
    }

    public boolean isCountryIdRequiredAlertDisplayed() {
        String alertText = alertMessage.getText();
        return alertText.contains("id_country is required.");
    }

    public boolean isCountryCannotBeLoadedWithAddressAlertDisplayed() {
        String alertText = alertMessage.getText();
        return alertText.contains("Country cannot be loaded with address->id_country");
    }

    public boolean isCountryInvalidAlertDisplayed() {
        String alertText = alertMessage.getText();
        return alertText.contains("Country is invalid");
    }

    public void clearField() {
        customerFirstName.clear();
        customerLastName.clear();
        password.clear();
        customerAddress.clear();
        customerCity.clear();
        customerPostcode.clear();
        customerMobilePhone.clear();
        driver.manage().deleteAllCookies();
    }
}
