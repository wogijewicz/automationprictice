package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Navigation;

public class OrderPage extends BasePage {

    @FindBy(css = ".cart_navigation .standard-checkout")
    WebElement proceedToCheckoutButton;

    @FindBy(css = ".cart_navigation .button-medium")
    WebElement proceedToCheckoutButtonAtAddressStep;

    @FindBy(id = "cgv")
    WebElement agreeToTheTermsBox;

    @FindBy(css = ".bankwire")
    WebElement payByBankWire;

    @FindBy(css = ".cheque")
    WebElement payByCheck;

    @FindBy(css = ".cart_navigation .button-medium")
    WebElement confirmMyOrderButton;

    @FindBy(css = ".cheque-indent .dark")
    WebElement orderIsCompleteInformationForBankWirePayment;

    @FindBy(css = ".alert-success")
    WebElement orderIsCompleteInformationForCheckPayment;

    public OrderPage(WebDriver driverIn, WebDriverWait waitIn) {
        super(driverIn, waitIn);
    }

    public void makeAnOrder() {
        ProductsPage productsPage = Navigation.goToProductCategoryPage(0, driver, wait);
        productsPage.addRandomProductToCart();
        productsPage.goToCartPage();
        wait.until(ExpectedConditions.elementToBeClickable(proceedToCheckoutButton)).click();
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.login(emailForSuccessfulLogin, passwordForSuccessfulLogin);
        wait.until(ExpectedConditions.elementToBeClickable(proceedToCheckoutButtonAtAddressStep)).click();
        agreeToTheTermsBox.click();
        wait.until(ExpectedConditions.elementToBeClickable(proceedToCheckoutButton)).click();
    }

    public void bankWirePaymentMethod() {
        wait.until(ExpectedConditions.elementToBeClickable(payByBankWire)).click();
        wait.until(ExpectedConditions.elementToBeClickable(confirmMyOrderButton)).click();
    }

    public void checkPaymentMethod() {
        wait.until(ExpectedConditions.elementToBeClickable(payByCheck)).click();
        wait.until(ExpectedConditions.elementToBeClickable(confirmMyOrderButton)).click();
    }

    public boolean isOrderCompleteIfBankWirePayment() {
        return orderIsCompleteInformationForBankWirePayment.getText().contains("Your order on My Store is complete.");
    }

    public boolean isOrderCompleteIfCheckPayment() {
        return orderIsCompleteInformationForCheckPayment.getText().contains("Your order on My Store is complete.");
    }
}