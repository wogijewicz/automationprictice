package pageobjects;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

public class MyWishlistsPage extends BasePage {

    @FindBy(xpath = "//a[contains(text(),'My wishlist')]")
    WebElement wishlistManageOnclick;

    @FindBy(css = ".wlp_bought_list .row")
    List<WebElement> productsNamesList;

    @FindBy(css = ".wishlist_delete .icon")
    List<WebElement> wishlistDeleteButton;

    public MyWishlistsPage(WebDriver driverIn, WebDriverWait waitIn) {
        super(driverIn, waitIn);
    }

    public void openMyWishlistManage() {
        wait.until(ExpectedConditions.elementToBeClickable(wishlistManageOnclick)).click();
    }

    public void deleteTheWishlist() {
        wishlistDeleteButton.get(0).click();
        wait.until(ExpectedConditions.alertIsPresent());
        Alert alert = driver.switchTo().alert();
        alert.accept();
    }

    public boolean isProductAtWishlist(String expectedProductName) {
        for (WebElement productsName : productsNamesList) {
            System.out.println(productsName.getText());
            if (productsName.getText().toLowerCase().contains(expectedProductName.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public void checkIfWishlistsEmpty() {
        MyAccountPage myAccountPage = new MyAccountPage(driver, wait);
        myAccountPage.goToWishlistsPage();
        if (wishlistDeleteButton.size()>0) {
            deleteTheWishlist();
        } else {
            System.out.println("Wishlists not found");
        }
    }
}