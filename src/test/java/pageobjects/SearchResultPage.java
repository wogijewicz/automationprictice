package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

public class SearchResultPage extends BasePage {

    @FindBy(css = ".product_list .product-name")
    List<WebElement> productsNamesList;

    @FindBy(css = ".heading-counter")
    WebElement searchSummary;

    @FindBy(css = "#center_column > .alert-warning")
    WebElement alertMessage;

    public SearchResultPage(WebDriver driverIn, WebDriverWait waitIn) {
        super(driverIn, waitIn);
    }

    public boolean isProductWithNameVisible(String expectedProductName) {
        for (WebElement productsName : productsNamesList) {
            System.out.println(productsName.getText());
            if (productsName.getText().toLowerCase().contains(expectedProductName.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public String getSearchSummary() {
        return searchSummary.getText();
    }

    public boolean isAlertTextCorrectWhenEnterKeyForNoExistingProduct() {
        String alertText = alertMessage.getText();
        return alertText.contains("No results were found for your search");
    }

    public boolean isAlertTextCorrectWhenNotEnterKeyForProduct() {
        String alertText = alertMessage.getText();
        return alertText.contains("Please enter a search keyword");
    }
}
