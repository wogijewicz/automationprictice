package pageobjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

public class BasePage {

    static WebDriver driver;
    static WebDriverWait wait;

    public String emailForSuccessfulLogin = "myTest2020@gmail.com";
    public String passwordForSuccessfulLogin = "1qaz!QAZ";

    @FindBy(id = "search_query_top")
    WebElement searchBox;

    @FindBy(css = ".menu-content>li>a")
    List<WebElement> productCategories;

    @FindBy(css = ".shopping_cart .ajax_cart_quantity")
    WebElement cartQuantity;

    @FindBy(css = ".login")
    WebElement goToLoginPageLink;

    @FindBy(css = ".account")
    WebElement goToMyAccountPageLink;

    @FindBy(css = ".logout")
    WebElement signOutButton;

    static final String BASE_URL = "http://automationpractice.com/";

    public BasePage(WebDriver driverIn, WebDriverWait waitIn) {
        this.driver = driverIn;
        this.wait = waitIn;
        PageFactory.initElements(driver, this);
    }

    public void searchForProduct(String productName) {
        searchBox.sendKeys(productName);
        searchBox.sendKeys(Keys.ENTER);
    }

    public void goToProductCategoryByIndex(int productCategoryByIndex) {
        wait.until(ExpectedConditions.elementToBeClickable(productCategories.get(productCategoryByIndex))).click();
    }

    public int getCartSize() {
        String cartQuantityText = cartQuantity.getText();
        return Integer.parseInt(cartQuantityText);
    }

    public LoginPage goToLoginPage() {
        wait.until(ExpectedConditions.elementToBeClickable(goToLoginPageLink)).click();
        return new LoginPage(driver, wait);
    }

    public void goToMyAccountPage() {
        wait.until(ExpectedConditions.elementToBeClickable(goToMyAccountPageLink)).click();
    }

    public LoginPage signOut() {
        wait.until(ExpectedConditions.elementToBeClickable(signOutButton)).click();
        return new LoginPage(driver, wait);
    }

    public boolean isSignInLinkDisplayed() {
        String loginPageButton = goToLoginPageLink.getText();
        return loginPageButton.equals("Sign in");
    }
}
