package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MyAccountPage extends BasePage {
    @FindBy(xpath = "//h1[@class='page-heading']")
    WebElement myAccountPageHeading;

    @FindBy(xpath = "//a[@title='My wishlists'] ")
    WebElement myWishlistsButton;

    @FindBy(xpath = "//a[@title='Information'] ")
    WebElement myPersonalInformationButton;

    @FindBy(id = "id_gender2")
    WebElement genderRadioBox;

    @FindBy(id = "firstname")
    WebElement customerFirstName;

    @FindBy(id = "lastname")
    WebElement customerLastName;

    @FindBy(id = "email")
    WebElement customerEmail;

    @FindBy(id = "days")
    WebElement customerDayOfBirth;

    @FindBy(id = "months")
    WebElement customerMonthOfBirth;

    @FindBy(id = "years")
    WebElement customerYearOfBirth;

    public MyAccountPage(WebDriver driverIn, WebDriverWait waitIn) {
        super(driverIn, waitIn);
    }

    public void myPersonalInformation() {
        wait.until(ExpectedConditions.elementToBeClickable(myPersonalInformationButton)).click();
    }

    public void goToWishlistsPage() {
        MyAccountPage myAccountPage = new MyAccountPage(driver, wait);
        myAccountPage.goToMyAccountPage();
        wait.until(ExpectedConditions.elementToBeClickable(myWishlistsButton)).click();
    }

    public boolean isUserLoginIn() {
        return myAccountPageHeading.getText().equalsIgnoreCase("My account");
    }

    public boolean isUserGenderDisplayCorrectly() {
        return genderRadioBox.isSelected();
    }

    public boolean isUserFirstNameDisplayCorrectly() {
        return customerFirstName.getAttribute("value").contains("Ann");
    }

    public boolean isUserLastNameDisplayCorrectly() {
        return customerLastName.getAttribute("value").contains("Kovalska");
    }

    public boolean isUserEmailDisplayCorrectly() {
        return customerEmail.getAttribute("value").contains("myTest2020@gmail.com");
    }

    public boolean isUserDayOfBirthDisplayCorrectly() {
        return customerDayOfBirth.getAttribute("value").contains("7");
    }

    public boolean isUserMonthOfBirthDisplayCorrectly() {
        return customerMonthOfBirth.getAttribute("value").contains("5");
    }

    public boolean isUserYearOfBirthDisplayCorrectly() {
        return customerYearOfBirth.getAttribute("value").contains("1995");
    }
}
