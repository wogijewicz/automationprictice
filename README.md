This project was crafted under Page Object Model applied to automationpractice.com. The goal is to practice and experiment with Selenium. 

[automationpractice.com](http://automationpractice.com/) is sample ecommerce website is being used by www.seleniumframework.com website to help pracitce exercises on a real-time ecommerce website. Different workflows for adding products to cart, checking out car, making payments etc.

### Tools: ###

* IntelliJ IDEA
* Java 11+
* Maven
* Selenium WebDriver
* jUnit 5.x
* Java Faker

### Tested functionalities: ###

* Create new account form
* Login form
* Complete order workflow
* Sign out
* Adding to Wishlist
* Keyword search
* My Account page 
* Adding to cart

### Author: ###

Viktoriia Ogievich